# Rest-Caller-API

API Rest that call other APIs and returns the information.

## Changelog

### 0.0.1 version  - Released 10/15/2019

- **Added**
  - Base project Spring Boot.
  - SL4J Logger.
  - Postman collections.