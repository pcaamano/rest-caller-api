package ar.com.pablocaamano.rest.caller.api.model;

public class Meta {

    private String method;
    private String operation;

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

}
