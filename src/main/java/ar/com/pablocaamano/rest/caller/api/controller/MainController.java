package ar.com.pablocaamano.rest.caller.api.controller;

import ar.com.pablocaamano.rest.caller.api.configuration.ApiConfiguration;
import ar.com.pablocaamano.rest.caller.api.model.Response;
import ar.com.pablocaamano.rest.caller.api.model.ServicesInformation;
import ar.com.pablocaamano.rest.caller.api.service.ApiService;
import ar.com.pablocaamano.rest.caller.api.util.ResponseBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping(value = "api")
public class MainController {

    private static final Logger logger = LoggerFactory.getLogger(MainController.class);

    @Autowired
    private ApiService apiService;

    @Autowired
    private ApiConfiguration apiConfiguration;


    @GetMapping(value = "health-check",produces = "application/json")
    public ResponseEntity<Response> checkHealth(HttpServletRequest httpRequest){
        logger.info("[" + apiConfiguration.getApplicationName() + "] - Calling service...");
        Map<String,String>dataResponse = apiService.getApiData();
        logger.info("[" + apiConfiguration.getApplicationName() + "] - Making response...");
        Response response = ResponseBuilder.init()
                .withMetaMethod(httpRequest.getMethod())
                .withMetaOperation(httpRequest.getRequestURI())
                .addData(dataResponse)
                .build();
        return new ResponseEntity(response, HttpStatus.OK);
    }

    @PostMapping(value = "/test-comunication")
    public ResponseEntity<Response> testComunication(
            @RequestBody ServicesInformation servicesInformation,
            HttpServletRequest httpRequest
    ){
        logger.info("[" + apiConfiguration.getApplicationName() + "] - Calling service for process information...");
        servicesInformation = apiService.addServiceInformation(servicesInformation);
        if (!servicesInformation.getSteps().isEmpty()){
            logger.info("[" + apiConfiguration.getApplicationName() + "] - Calling service to access next step...");
            servicesInformation = apiService.nextStep(servicesInformation);
        }
        Response response = ResponseBuilder.init()
                .withMetaMethod(httpRequest.getMethod())
                .withMetaOperation(httpRequest.getRequestURI())
                .addData(servicesInformation.getInformation())
                .build();
        return new ResponseEntity<>(response,HttpStatus.OK);
    }
}
