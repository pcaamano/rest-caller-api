package ar.com.pablocaamano.rest.caller.api.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(value = "ar.com.pablocaamano.api.test")
public class ApiConfiguration {

    @Value(value = "${application.name}")
    private String applicationName;

    public String getApplicationName() {
        return applicationName;
    }

}
