package ar.com.pablocaamano.rest.caller.api.model;

import org.springframework.http.HttpMethod;

public class Step {
    private String host;
    private String path;
    private String httpMethod;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public HttpMethod getHttpMethod() {
        if(httpMethod.equals("POST")){
            return HttpMethod.POST;
        }
        return null;
    }

    public void setHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
    }
}
