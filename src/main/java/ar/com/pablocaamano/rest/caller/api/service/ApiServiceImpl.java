package ar.com.pablocaamano.rest.caller.api.service;

import ar.com.pablocaamano.rest.caller.api.configuration.ApiConfiguration;
import ar.com.pablocaamano.rest.caller.api.model.Response;
import ar.com.pablocaamano.rest.caller.api.model.ServicesInformation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Service
public class ApiServiceImpl implements ApiService {

    private static final Logger logger = LoggerFactory.getLogger(ApiServiceImpl.class);

    @Autowired
    private ApiConfiguration apiConfiguration;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private HttpServletRequest request;

    @Override
    public Map<String, String> getApiData() {
        logger.info("[" + apiConfiguration.getApplicationName() + "] - Building data...");
        Map<String,String>dataResponse = new HashMap<>();
        dataResponse.put("application_name",apiConfiguration.getApplicationName());
        dataResponse.put("status","active");
        return dataResponse;
    }

    @Override
    public ServicesInformation addServiceInformation(ServicesInformation servicesInformation) {
        logger.info("[" + apiConfiguration.getApplicationName() + "] - Building data...");
        Map<String,String>information = new HashMap<>();
        information.put("application_name",apiConfiguration.getApplicationName());
        information.put("method",request.getMethod());
        information.put("request_uri",request.getRequestURI());
        if (servicesInformation.getInformation() == null){
            List<Map<String,String>>informationList = new LinkedList<>();
            informationList.add(information);
            servicesInformation.setInformation(informationList);
        }else {
            List<Map<String, String>> informationList = servicesInformation.getInformation();
            informationList.add(information);
            servicesInformation.setInformation(informationList);
        }
        return servicesInformation;
    }

    @Override
    public ServicesInformation nextStep(ServicesInformation servicesInformation) {
        String nextHost = servicesInformation.getSteps().get(0).getHost();
        String nextPath = servicesInformation.getSteps().get(0).getPath();
        HttpMethod nextMethod = servicesInformation.getSteps().get(0).getHttpMethod();
        servicesInformation.getSteps().remove(0);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<ServicesInformation> request = new HttpEntity<>(servicesInformation,headers);
        logger.info("[" + apiConfiguration.getApplicationName() + "] - Calling next step...");
        ResponseEntity<Response> response = restTemplate.exchange(
                nextHost + nextPath,
                nextMethod,
                request,
                Response.class
        );
        ServicesInformation informationResponse = new ServicesInformation();
        informationResponse.setInformation((List<Map<String, String>>) response.getBody().getData());
        return informationResponse;
    }
}
