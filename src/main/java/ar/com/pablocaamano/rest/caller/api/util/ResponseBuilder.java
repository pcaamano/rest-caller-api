package ar.com.pablocaamano.rest.caller.api.util;

import ar.com.pablocaamano.rest.caller.api.model.Meta;
import ar.com.pablocaamano.rest.caller.api.model.Response;
import ar.com.pablocaamano.rest.caller.api.model.Error;

import java.util.ArrayList;
import java.util.List;

public class ResponseBuilder {

    private Response response;
    private Meta meta;
    private Object data;
    private List<Error> errorList;

    public ResponseBuilder(){
        response = new Response();
        meta = new Meta();
        data = new ArrayList<>();
        errorList = new ArrayList<>();
    }

    public static ResponseBuilder init(){
        return new ResponseBuilder();
    }

    public ResponseBuilder withMetaMethod(String method){
        this.meta.setMethod(method);
        return this;
    }

    public ResponseBuilder withMetaOperation(String operation){
        this.meta.setOperation(operation);
        return this;
    }

    public ResponseBuilder addData(Object dataElem){
        this.data = dataElem;
        return this;
    }

    public ResponseBuilder addError(Error error){
        this.errorList.add(error);
        return this;
    }

    public Response build(){
        this.response.setMeta(meta);
        this.response.setData(data);
        this.response.setErrors(errorList);
        return this.response;
    }
}
