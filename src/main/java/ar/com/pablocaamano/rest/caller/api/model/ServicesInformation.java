package ar.com.pablocaamano.rest.caller.api.model;

import java.util.List;
import java.util.Map;

public class ServicesInformation {
    private List<Step> steps;
    private List<Map<String,String>> information;

    public List<Step> getSteps() {
        return steps;
    }

    public void setSteps(List<Step> steps) {
        this.steps = steps;
    }

    public List<Map<String, String>> getInformation() {
        return information;
    }

    public void setInformation(List<Map<String, String>> information) {
        this.information = information;
    }
}
