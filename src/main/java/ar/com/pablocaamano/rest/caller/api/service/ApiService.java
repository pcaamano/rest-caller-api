package ar.com.pablocaamano.rest.caller.api.service;

import ar.com.pablocaamano.rest.caller.api.model.ServicesInformation;

import java.util.Map;

public interface ApiService {
    Map<String,String> getApiData();
    ServicesInformation addServiceInformation(ServicesInformation servicesInformation);
    ServicesInformation nextStep(ServicesInformation servicesInformation);
}
